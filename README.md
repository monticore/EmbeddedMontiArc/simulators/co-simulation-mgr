<!-- (c) https://github.com/MontiCore/monticore -->
# Co-Simulation Manager
[![pipeline status](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/co-simulation-mgr/badges/master/pipeline.svg)](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/co-simulation-mgr/commits/master)

The Co-Simulation Manager project is responsible for three fundamental task regarding a co-simulation:
1. It provides the interface for `CoSimulators` which shall be inluded in a co-simulation setup.
2. It provides the interface for `Execution` classes. These classes will be generated during the co-simulation generation step.
3. It is responsible for the correct execution of a co-simulation:
    * Time management
    * Flag control
    * Step size search

The Manager artifacts are the foundation of the [Co-Simulation Generator](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/co-simulation-gen).
