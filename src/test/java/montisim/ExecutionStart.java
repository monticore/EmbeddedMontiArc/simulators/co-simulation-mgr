/* (c) https://github.com/MontiCore/monticore */
package montisim;

public class ExecutionStart {

	public static void main(String[] args) {
		SimExecutor simExecutor = new SimExecutor(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
		Execution execution = new ConcreteExecution();
		
		simExecutor.setExecution(execution);
		simExecutor.execute();
	}
}
