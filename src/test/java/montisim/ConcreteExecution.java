/* (c) https://github.com/MontiCore/monticore */

package montisim;

public class ConcreteExecution implements Execution{
	
	Sim1 sim1 = new Sim1();
	Sim2 sim2 = new Sim2();
	Sim3 sim3 = new Sim3();

	@Override
	public void executeAll(double deltaTime) {

		sim1.setDeltaTime(deltaTime);

		sim1.execute();
		sim2.setDeltaTime(deltaTime);

		sim2.execute();
		sim3.setDeltaTime(deltaTime);
		sim3.setin1(sim1.getout1());
		sim3.setin2(sim2.getout1());

		sim3.execute();
	}
	

	@Override
	public String getCurOutput() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("res: ");
		stringBuilder.append(sim3.getout1());
		return stringBuilder.toString();
	}

	@Override
	public boolean checkFlags() {
		boolean flag = false;

		flag = flag || sim1.getFlag();
		flag = flag || sim2.getFlag();
		flag = flag || sim3.getFlag();

		return flag;
	}

	@Override
	public void saveAllStates() {
		sim1.saveState();
		sim2.saveState();
		sim3.saveState();
	}

	@Override
	public void loadAllStates() {
		sim1.loadState();
		sim2.loadState();
		sim3.loadState();
	}


    @Override
    public String getSimulationData() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(sim3.getout1());
        return stringBuilder.toString();
    }


    @Override
    public String getOutputNames() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("res" + ";");
        return stringBuilder.toString();
    }
    
}
