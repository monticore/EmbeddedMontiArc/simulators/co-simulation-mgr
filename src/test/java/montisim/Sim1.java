/* (c) https://github.com/MontiCore/monticore */
package montisim;

import montisim.CoSimulator;

public class Sim1 implements CoSimulator{
	
	private double deltaTime;
	private boolean flag = false;
	
	private double out1;
	private double internValue = 0;
	private double internTime = 0;
	
	private double saveOut1;
	private double saveInternValue;
	private double saveinternTime;
	
	public Sim1() {}
	
	@Override
	public void execute() {
		out1 = deltaTime * internValue;
		internValue = internValue + 0.1;
		
		internTime = internTime + deltaTime;
		if (internTime > 8 && deltaTime > 0.05) {
			flag = true;
		} else flag = false;
	}

	@Override
	public boolean getFlag() {
		return flag;
	}

	@Override
	public void saveState() {
		saveOut1 = out1;
		saveInternValue = internValue;
		saveinternTime = internTime;
	}

	@Override
	public void loadState() {
		out1 = saveOut1;
		internValue = saveInternValue;
		internTime = saveinternTime;
	}
	
	public double getout1() {
		return out1;
	}

	@Override
	public void setDeltaTime(double deltaTime) {
		this.deltaTime = deltaTime;
		
	}
}
