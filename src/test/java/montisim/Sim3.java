/* (c) https://github.com/MontiCore/monticore */
package montisim;

import montisim.CoSimulator;

public class Sim3 implements CoSimulator{
	
	@SuppressWarnings("unused")
	private double deltaTime;
	private double in1;
	private double in2;
	private double out1;
	
	private double saveIn1;
	private double saveIn2;
	private double saveOut1;
	
	public Sim3() {}
	
	@Override
	public void execute() {
		out1 = in1 + in2;
	}

	@Override
	public boolean getFlag() {
		return false;
	}

	@Override
	public void saveState() {
		saveIn1 = in1;
		saveIn2 = in2;
		saveOut1 = out1;
	}

	@Override
	public void loadState() {
		in1 = saveIn1;
		in2 = saveIn2;
		out1 = saveOut1;
	}
	
	public void setin1(double in1) {
		this.in1 = in1;
	}
	
	public void setin2(double in2) {
		this.in2 = in2;
	}
	
	public double getout1() {
		return out1;
	}

	@Override
	public void setDeltaTime(double deltaTime) {
		this.deltaTime = deltaTime;
		
	}
}
