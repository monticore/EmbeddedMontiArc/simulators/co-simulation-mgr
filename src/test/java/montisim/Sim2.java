/* (c) https://github.com/MontiCore/monticore */
package montisim;

import montisim.CoSimulator;

public class Sim2 implements CoSimulator{
	
	private double deltaTime;
	private boolean flag = false;
	
	private double out1;
	private double internTime = 0;
	private double internValue = 0;
	
	private double saveOut1;
	private double saveInternTime;
	private double saveInternValue;
	
	public Sim2() {}
	
	@Override
	public void execute() {
		out1 = deltaTime * internValue;
		if (internValue < 50) internValue++;
		
		internTime = internTime + deltaTime;
		if (internTime > 16 && deltaTime > 0.01) {
			flag = true;
		} else flag = false;
	}

	@Override
	public boolean getFlag() {
		return flag;
	}

	@Override
	public void saveState() {
		saveOut1 = out1;
		saveInternValue = internValue;
		saveInternTime = internTime;
	}

	@Override
	public void loadState() {
		out1 = saveOut1;
		internValue = saveInternValue;
		internTime = saveInternTime;
	}
	
	public double getout1() {
		return out1;
	}
	
	public void setDeltaTime(double deltaTime) {
		this.deltaTime = deltaTime;
	}
}
