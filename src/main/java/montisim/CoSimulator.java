/* (c) https://github.com/MontiCore/monticore */
/**
 * 
 */
package montisim;

/**
 * This is the interface for all future co-simulators.
 * Every simulator which wants to be integrated in co-simulation setup has to implement this interface.
 * 
 * In order to be accessible at runtime the following conventions need to be fulfilled:
 * - Every input/output variable need a getter/setter method with following naming convention: 
 *      public <Datatype> get<name>()
 *      -> Example: public int getout1()
 *      public void set<name>(<Datatype> <ParameterName>)
 *      -> Example: public void setin1(String myName)
 * - Notice that in both examples the names must be identical to the variable names(e.g. lower case)!
 * - The names may not contain the string 'vehicle'!
 * - A non-parametric constructor
 * - A time input variable which can be used in the execute method and is updated via setDeltaTime(...)
 * 
 * The getFlag method provides the possibility to inform the SimManager that a more fine-grained time
 * resolution is necessary.
 * 
 * Conventions for the corresponding ema file:
 * - For every input/output variable there is an input/output port which has the same name as the corresponding variable
 * - There is one input port with the name deltaTime which is connected to the input port of the top component
 * 
 *
 */
public interface CoSimulator {
    
    public void execute();
    
    public void setDeltaTime(double deltaTime);
    
    public boolean getFlag();
    
    public void saveState();
    
    public void loadState();
}
