/* (c) https://github.com/MontiCore/monticore */
/**
 * 
 */
package montisim;

/**
 * The Execution interface that will be implemented by the generated execution
 * classes. It handles the initiation and communication between simulators at
 * runtime.
 * 
 *
 */
public interface Execution {

    public void executeAll(double deltaTime);

    public String getCurOutput();

    public boolean checkFlags();

    public void saveAllStates();

    public void loadAllStates();
    
    public String getSimulationData();

    public String getOutputNames();
}
