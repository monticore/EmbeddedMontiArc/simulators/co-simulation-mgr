/* (c) https://github.com/MontiCore/monticore */
/**
 * 
 */
package montisim;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SimExecutor {

    private static final Logger LOGGER = LogManager.getLogger(SimExecutor.class);

    private double curTime = 0;
    // Duration of the setup in seconds
    private double endTime;
    // Loops per second in Hertz
    private double loopFrequency;
    private double stepSize;

    private int counter = 0;
    private static final int INTERVAL = 10;
    private static final double RESOLUTION = 0.001;
    private static final double MIN_STEPSIZE = 0.000001;
    private static final double MAX_STEPSIZE = 5.0;

    // Execution instance which will be implemented by the generated class
    private Execution execution;

    private DecimalFormat decimalFormat = new DecimalFormat("#.#####");
    private BufferedWriter bufferedWriter;

    private boolean isFirstExecution = true;

    public SimExecutor(double endTime, double loopFrequency) {
        LOGGER.debug("Create SimExecutor instance.");
        prepareDataWriter();
        this.endTime = endTime;
        this.loopFrequency = loopFrequency;
        updateStepSize();
    }

    private void prepareDataWriter() {
        try {
            bufferedWriter = new BufferedWriter(new FileWriter("target/SimulationData.csv"));
        } catch (IOException e) {
            LOGGER.warn("Could not initiate data writer.", e);
        }
    }

    private void updateStepSize() {
        stepSize = 1.0 / loopFrequency;
    }

    public void execute() {
        if (execution == null) {
            LOGGER.fatal("The execution object is not initialized. Set it to generated execution instance.",
                    new Exception("No execution object"));
        }

        LOGGER.info("Begin simulation");
        for (; curTime <= endTime; curTime = curTime + stepSize) {
            LOGGER.info("Time: " + decimalFormat.format(curTime) + "; Output: " + execution.getCurOutput());
            logSimulationData();
            execution.executeAll(stepSize);
            counter++;
            if (counter % INTERVAL == 0) {
                checkForFlags();
                counter = 0;
            }
        }
        
        try {
            bufferedWriter.close();
        } catch (IOException e) {
            LOGGER.warn("Could not close the simulation writer properly.", e);
        }
    }

    private void logSimulationData() {
        try {
            if (isFirstExecution) {
                bufferedWriter.write(execution.getOutputNames());
                bufferedWriter.newLine();
                isFirstExecution = false;
            }
            bufferedWriter.write(execution.getSimulationData());
            bufferedWriter.newLine();
        } catch (IOException e) {
            LOGGER.warn("Could not write simulation data.", e);
        }
    }

    private void checkForFlags() {
        LOGGER.trace("Check for flags.");
        if (execution.checkFlags()) {
            LOGGER.debug("Time flag was set.");
            execution.saveAllStates();
            LOGGER.info("Old step size: " + stepSize);
            findStepSize();
            LOGGER.info("New step size " + stepSize);
            execution.loadAllStates();
        }
    }

    private void findStepSize() {
        double lowerValue = stepSize;
        double higherValue = stepSize;
        while (execution.checkFlags()) {
            higherValue = lowerValue;
            lowerValue = lowerValue / 2;
            execution.executeAll(lowerValue);
            if (lowerValue < MIN_STEPSIZE) {
                LOGGER.warn("Minimal step size is reached and flags are still set. Continuing with minimal step size.");
                lowerValue = MIN_STEPSIZE;
                return;
            }
        }
        LOGGER.info("LowerValue: " + lowerValue + " and HigherValue: " + higherValue);
        findGreaterStepSize(lowerValue, higherValue);
    }

    private void findGreaterStepSize(double lowerValue, double higherValue) {
        LOGGER.info("lowerValue: " + lowerValue + " and higherValue: " + higherValue);
        if (Math.abs(higherValue - lowerValue) < RESOLUTION) {
            stepSize = lowerValue;
            return;
        } else {
            double oldHigherValue = higherValue;
            higherValue = (lowerValue + higherValue) / 2;
            execution.executeAll(higherValue);
            if (execution.checkFlags()) {
                findGreaterStepSize(lowerValue, higherValue);
            } else {
                findGreaterStepSize(higherValue, oldHigherValue);
            }

            if (stepSize > MAX_STEPSIZE) {
                LOGGER.warn("Maximal step size is reached and flags are not set. Continuing with maximal step size.");
                stepSize = MAX_STEPSIZE;
                return;
            }
        }
    }

    public void setExecution(Execution execution) {
        this.execution = execution;
    }
}
